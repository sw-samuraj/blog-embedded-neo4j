# Embedded Neo4j #

An example project for demonstration of [Neo4j](https://neo4j.com/) used
as an embedded data storage for data received via REST service
and propagated to [Wicket](http://wicket.apache.org/) GUI via *WebSockets*.

## How to run the example

1. Clone the repository.
1. Run the command `gradle tomcatRun`.
1. Open a browser on URL <http://localhost:4040/deep-thought> to see a page without data.
1. Open *SoapUI* project from `src/test/soapui`.
1. Run the test suite *Cases + Persons + Documents*.
1. Check the browser with newly loaded data.
1. Check the log in the console.
1. **Browse the source code.**


## What if you're sitting behind a proxy?

You need to modify (or create) your `$HOME/.gradle/gradle.properties` in
following way:

```
#!properties
systemProp.http.proxyHost=<your-proxy-host>
systemProp.http.proxyPort=<your-proxy-port>
systemProp.http.nonProxyHosts=localhost|<other-non-proxy-host>
systemProp.https.proxyHost=<your-proxy-host>
systemProp.https.proxyPort=<your-proxy-port>
systemProp.https.nonProxyHosts=localhost|<other-non-proxy-host>
```

## License ##

The **blog-embedded-neo4j** project is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
