package cz.swsamuraj.neo4j.person.graph;

import cz.swsamuraj.neo4j.document.graph.DocumentNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@NodeEntity(label = "Person")
public class PersonNode {

    @GraphId
    private Long id;

    private String caseID;

    private String personID;

    private String firstName;

    private String lastName ;

    private String sex;

    private String timestamp;

    @Relationship(type = "REQUESTED")
    public Set<DocumentNode> documents;

    public void requested(DocumentNode document) {
        if (documents == null) {
            documents = new HashSet<>();
        }

        documents.add(document);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseID() {
        return caseID;
    }

    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setTimestampAsString(OffsetDateTime timestamp) {
        this.timestamp = timestamp.toString();
    }

}
