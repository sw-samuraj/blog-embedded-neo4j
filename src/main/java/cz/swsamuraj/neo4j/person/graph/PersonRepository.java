package cz.swsamuraj.neo4j.person.graph;

import org.springframework.data.neo4j.repository.GraphRepository;

public interface PersonRepository extends GraphRepository<PersonNode> {

    PersonNode findByPersonID(String personID);

}
