package cz.swsamuraj.neo4j.person.graph;

import cz.swsamuraj.neo4j.ObservableCoordinator;
import cz.swsamuraj.neo4j.caze.graph.CaseNode;
import cz.swsamuraj.neo4j.caze.graph.CaseRepository;
import cz.swsamuraj.neo4j.person.gui.PersonEvent;
import cz.swsamuraj.neo4j.person.logic.PersonService;
import cz.swsamuraj.neo4j.ws.model.PersonInfo;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphPersonService implements PersonService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CaseRepository caseRepository;

    @Autowired
    private ObservableCoordinator observable;

    @Override
    public Long storePerson(PersonInfo personInfo) {
        PersonNode personNode = mapper.map(personInfo, PersonNode.class);
        Long id = personRepository.save(personNode).getId();
        personNode = personRepository.findOne(id);

        CaseNode caseNode = caseRepository.findByCaseID(personNode.getCaseID());
        caseNode.contains(personNode);
        caseRepository.save(caseNode);

        PersonEvent event = new PersonEvent(personNode.getPersonID());
        observable.notify(event);

        return id;
    }
}
