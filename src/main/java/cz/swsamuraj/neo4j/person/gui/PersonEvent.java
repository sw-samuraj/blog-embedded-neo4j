package cz.swsamuraj.neo4j.person.gui;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

public class PersonEvent implements IWebSocketPushMessage {

    private String personID;

    public PersonEvent(String personID) {
        this.personID = personID;
    }

    public String getPersonID() {
        return personID;
    }

    @Override
    public String toString() {
        return String.format("PersonEvent[personID: %s]", personID);
    }
}
