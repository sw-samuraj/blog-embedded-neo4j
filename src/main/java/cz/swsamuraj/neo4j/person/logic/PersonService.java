package cz.swsamuraj.neo4j.person.logic;

import cz.swsamuraj.neo4j.ws.model.PersonInfo;

public interface PersonService {

    Long storePerson(PersonInfo person);

}
