package cz.swsamuraj.neo4j.person.logic;

import cz.swsamuraj.neo4j.ws.api.PersonApi;
import cz.swsamuraj.neo4j.ws.model.PersonInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class PersonApiController implements PersonApi {

    private static final Logger logger = LogManager.getLogger(PersonApiController.class);

    @Autowired
    private PersonService personService;

    @Override
    public ResponseEntity<Void> putPerson(@RequestBody PersonInfo person) {
        logger.debug("PersonInfo:\n" + person);

        Long id = personService.storePerson(person);

        logger.debug("PersonNode[ID: {}]", id);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
