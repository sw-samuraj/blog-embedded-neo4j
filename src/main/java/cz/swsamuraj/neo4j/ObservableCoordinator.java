package cz.swsamuraj.neo4j;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

import java.util.Observer;

public interface ObservableCoordinator {

    void registerObserver(Observer observer);

    void notify(IWebSocketPushMessage event);

}
