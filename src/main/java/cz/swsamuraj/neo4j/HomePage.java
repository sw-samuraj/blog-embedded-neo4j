package cz.swsamuraj.neo4j;

import cz.swsamuraj.neo4j.caze.gui.CasePanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class HomePage extends WebPage {

    private static final Logger logger = LogManager.getLogger(HomePage.class);

    public HomePage() {
        add(new Label("helloLabel", "Hello, Wicket & Spring & Neo4j!"));
        add(new CasePanel("casePanel"));
    }

}
