package cz.swsamuraj.neo4j.caze.gui;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

public class CaseEvent implements IWebSocketPushMessage {

    private String caseID;

    public CaseEvent(String caseID) {
        this.caseID = caseID;
    }

    public String getCaseID() {
        return caseID;
    }

    @Override
    public String toString() {
        return String.format("CaseEvent[caseID: %s]", caseID);
    }
}
