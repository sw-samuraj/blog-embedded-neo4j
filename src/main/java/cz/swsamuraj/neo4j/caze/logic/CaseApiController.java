package cz.swsamuraj.neo4j.caze.logic;

import cz.swsamuraj.neo4j.ws.api.CaseApi;
import cz.swsamuraj.neo4j.ws.model.CaseInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class CaseApiController implements CaseApi {

    private static final Logger logger = LogManager.getLogger(CaseApiController.class);

    @Autowired
    private CaseService caseService;

    @Override
    public ResponseEntity<Void> putCase(@RequestBody CaseInfo caseInfo) {
        logger.debug("CaseInfo:\n" + caseInfo);

        Long id = caseService.storeCase(caseInfo);

        logger.debug("CaseNode[ID: {}]", id);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

}
