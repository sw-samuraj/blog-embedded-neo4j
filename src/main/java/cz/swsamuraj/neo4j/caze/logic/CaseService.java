package cz.swsamuraj.neo4j.caze.logic;

import cz.swsamuraj.neo4j.ws.model.CaseInfo;

public interface CaseService {

    Long storeCase(CaseInfo caseInfo);

}
