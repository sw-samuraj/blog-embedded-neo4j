package cz.swsamuraj.neo4j.caze.graph;

import org.springframework.data.neo4j.repository.GraphRepository;

public interface CaseRepository extends GraphRepository<CaseNode> {

   CaseNode findByCaseID(String caseID);

}
