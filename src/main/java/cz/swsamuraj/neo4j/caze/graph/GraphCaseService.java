package cz.swsamuraj.neo4j.caze.graph;

import cz.swsamuraj.neo4j.ObservableCoordinator;
import cz.swsamuraj.neo4j.caze.gui.CaseEvent;
import cz.swsamuraj.neo4j.caze.logic.CaseService;
import cz.swsamuraj.neo4j.ws.model.CaseInfo;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphCaseService implements CaseService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private CaseRepository repository;

    @Autowired
    private ObservableCoordinator observable;

    @Override
    public Long storeCase(CaseInfo caseInfo) {
        CaseNode caseNode = mapper.map(caseInfo, CaseNode.class);
        Long id = repository.save(caseNode).getId();

        CaseEvent event = new CaseEvent(caseNode.getCaseID());
        observable.notify(event);

        return id;
    }

}
