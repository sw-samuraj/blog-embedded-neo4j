package cz.swsamuraj.neo4j.caze.graph;

import cz.swsamuraj.neo4j.person.graph.PersonNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@NodeEntity(label = "Case")
public class CaseNode {

    @GraphId
    private Long id;

    private String caseID;

    private String timestamp;

    @Relationship(type = "CONTAINS")
    public Set<PersonNode> persons;

    public void contains(PersonNode person) {
        if (persons == null) {
            persons = new HashSet<>();
        }

        persons.add(person);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseID() {
        return caseID;
    }

    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setTimestampAsString(OffsetDateTime timestamp) {
        this.timestamp = timestamp.toString();
    }

}
