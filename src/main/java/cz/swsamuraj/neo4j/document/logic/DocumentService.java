package cz.swsamuraj.neo4j.document.logic;

import cz.swsamuraj.neo4j.ws.model.DocumentInfo;

public interface DocumentService {

    Long storeDocument(DocumentInfo document);

}
