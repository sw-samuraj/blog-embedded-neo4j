package cz.swsamuraj.neo4j.document.graph;

import cz.swsamuraj.neo4j.ws.model.DocumentType;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.typeconversion.EnumString;

@NodeEntity(label = "Document")
public class DocumentNode {

    @GraphId
    private Long id;

    private String personID;

    private String documentID;

    @EnumString(value = DocumentType.class)
    private DocumentType documentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

}
