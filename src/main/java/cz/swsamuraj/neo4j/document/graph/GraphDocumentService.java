package cz.swsamuraj.neo4j.document.graph;

import cz.swsamuraj.neo4j.document.logic.DocumentService;
import cz.swsamuraj.neo4j.person.graph.PersonNode;
import cz.swsamuraj.neo4j.person.graph.PersonRepository;
import cz.swsamuraj.neo4j.ws.model.DocumentInfo;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphDocumentService implements DocumentService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    public Long storeDocument(DocumentInfo document) {
        DocumentNode documentNode = mapper.map(document, DocumentNode.class);
        Long id = documentRepository.save(documentNode).getId();
        documentNode = documentRepository.findOne(id);

        PersonNode personNode = personRepository.findByPersonID(documentNode.getPersonID());
        personNode.requested(documentNode);
        personRepository.save(personNode);

        return id;
    }

}
