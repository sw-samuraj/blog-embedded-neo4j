package cz.swsamuraj.neo4j.document.graph;

import org.springframework.data.neo4j.repository.GraphRepository;

public interface DocumentRepository extends GraphRepository<DocumentNode> {

    DocumentNode findByDocumentID(String documentID);

}
