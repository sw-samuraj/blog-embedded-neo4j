package cz.swsamuraj.neo4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletRegistration;

@Component
public class WicketApplication extends WebApplication {

    private static final Logger logger = LogManager.getLogger(WicketApplication.class);

    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    @Override
    protected void init() {
        super.init();

        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx));

        ServletRegistration.Dynamic dispatcherServlet = getServletContext().addServlet(
                "dispatcherServlet",
                new DispatcherServlet(ctx));
        dispatcherServlet.setLoadOnStartup(1);
        dispatcherServlet.addMapping("/rest/*");
    }

}
