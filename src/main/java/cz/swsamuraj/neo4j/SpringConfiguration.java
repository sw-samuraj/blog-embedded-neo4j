package cz.swsamuraj.neo4j;

import org.dozer.DozerBeanMapper;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.ogm.driver.Driver;
import org.neo4j.ogm.drivers.embedded.driver.EmbeddedDriver;
import org.neo4j.ogm.service.Components;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan("cz.swsamuraj.neo4j")
@EnableNeo4jRepositories
public class SpringConfiguration {

    @Bean(destroyMethod = "shutdown")
    public GraphDatabaseService graphDatabaseService() {
        return new GraphDatabaseFactory().newEmbeddedDatabase(new File("build/neo4j"));
    }

    @Bean
    public SessionFactory getSessionFactory() {
        Driver driver = new EmbeddedDriver(graphDatabaseService());
        Components.setDriver(driver);

        return new SessionFactory("cz.swsamuraj.neo4j.caze.graph",
                "cz.swsamuraj.neo4j.person.graph",
                "cz.swsamuraj.neo4j.document.graph");
    }

    @Bean
    public Neo4jTransactionManager transactionManager() {
        return new Neo4jTransactionManager(getSessionFactory());
    }

    /*
    @Bean
    public org.neo4j.ogm.config.Configuration getConfiguration() {
        org.neo4j.ogm.config.Configuration configuration = new org.neo4j.ogm.config.Configuration();
        configuration.driverConfiguration()
                .setDriverClassName("org.neo4j.ogm.drivers.bolt.driver.BoltDriver")
                .setURI("bolt://neo4j:neo4j@localhost");

        return configuration;
    }

    @Bean
    public SessionFactory getSessionFactory() {
        return new SessionFactory(getConfiguration(),
                "cz.swsamuraj.neo4j.caze.graph",
                "cz.swsamuraj.neo4j.person.graph",
                "cz.swsamuraj.neo4j.document.graph");
    }
    */

    @Bean
    public ObservableCoordinator getObservable() {
        return new ObservableCoordinatorFacade();
    }

    @Bean
    public DozerBeanMapper dozerMapper() {
        List<String> mappingFiles = Arrays.asList("dozer-bean-mappings.xml");
        DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);

        return dozerBean;
    }

    @Bean
    public AnnotationMethodHandlerAdapter annotationMethodHandlerAdapter() {

        AnnotationMethodHandlerAdapter adapter = new AnnotationMethodHandlerAdapter();
        HttpMessageConverter<?>[] converters = { new MappingJackson2HttpMessageConverter()};
        adapter.setMessageConverters(converters);

        return  adapter;
    }

}
