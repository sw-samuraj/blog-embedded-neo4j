package cz.swsamuraj.neo4j;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

import java.util.Observable;
import java.util.Observer;

public class ObservableCoordinatorFacade extends Observable implements ObservableCoordinator {

    @Override
    public void registerObserver(Observer observer) {
        addObserver(observer);
    }

    @Override
    public void notify(IWebSocketPushMessage event) {
        setChanged();
        notifyObservers(event);
    }

}
